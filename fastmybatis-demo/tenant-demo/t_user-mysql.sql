CREATE DATABASE IF NOT EXISTS `stu` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
use stu;

DROP TABLE IF EXISTS `tenant_demo`;
CREATE TABLE `tenant_demo`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `tenant_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户id',
                                `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '多租户表演示' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_demo
-- ----------------------------
INSERT INTO `tenant_demo` VALUES (1, 'a100001', 'Jim');
INSERT INTO `tenant_demo` VALUES (2, 'a100002', 'Tom');
INSERT INTO `tenant_demo` VALUES (3, 'b100003', 'Tim');