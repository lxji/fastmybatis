package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.UserLog;


/**
 * @author tanghc
 */
public interface UserLogMapper extends CrudMapper<UserLog, Long> {
}
