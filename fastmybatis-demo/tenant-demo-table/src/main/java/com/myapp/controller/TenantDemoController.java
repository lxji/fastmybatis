package com.myapp.controller;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.param.PageParam;
import com.myapp.entity.UserLog;
import com.myapp.service.UserLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 演示多租户
 * <pre>
 *     关键配置在WebConfig、TenantInterceptor
 * </pre>
 * @author xxx
 */
@RestController
@RequestMapping("tenant")
public class TenantDemoController {

    @Autowired
    private UserLogService userLogService;

    /**
     * 分页查询
     * <p>
     *     http://localhost:8282/tenant/page?pageIndex=1&tenantId=a10001
     * </p>
     * @param param
     * @return
     */
    @GetMapping("/page")
    public PageInfo<UserLog> page(PageParam param) {
        Query query = param.toTenantQuery();
        return userLogService.page(query);
    }

    /**
     * http://localhost:8282/tenant/list?tenantId=a10001
     * http://localhost:8282/tenant/list?tenantId=a10001&userId=111
     * @return
     */
    @GetMapping("/list")
    public List<UserLog> list(@RequestParam(required = false) Integer userId) {
        // 使用TenantQuery表示使用了多租户查询
        Query query = new Query();
        // 添加其它查询条件
        query.eq(userId != null, "user_id", userId);
        return userLogService.list(query);
    }


    
}