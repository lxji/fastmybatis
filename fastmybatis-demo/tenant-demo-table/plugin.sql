
CREATE DATABASE IF NOT EXISTS `stu` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE `stu`;




DROP TABLE IF EXISTS `user_log_a10001`;
DROP TABLE IF EXISTS `user_log_a10002`;
DROP TABLE IF EXISTS `user_log_a10003`;
DROP TABLE IF EXISTS `user_log_a10004`;


CREATE TABLE `user_log_a10004` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_log_a10003` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_log_a10002` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_log_a10001` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

