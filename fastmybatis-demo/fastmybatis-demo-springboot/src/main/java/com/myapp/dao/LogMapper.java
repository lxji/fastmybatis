package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.gitee.fastmybatis.core.query.Query;
import com.myapp.entity.Log;
import com.myapp.entity.TUser;
import com.myapp.entity.UserInfoDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface LogMapper extends CrudMapper<Log, Void> {


}