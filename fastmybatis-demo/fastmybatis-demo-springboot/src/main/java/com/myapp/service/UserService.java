package com.myapp.service;

import com.gitee.fastmybatis.core.support.IService;
import com.myapp.entity.TUser;
import org.springframework.stereotype.Service;

/**
 * 用户service
 * @author thc
 */
@Service
public class UserService implements IService<TUser/*实体类*/, Integer/*主键类型*/> {

}
