package com.myapp.entity;


import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;



/**
 * 表名：address
 *
 * @author xxx
 */
@Table(name = "address", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class Address {

    /**  数据库字段：id */
    private String id;

    /**  数据库字段：address */
    private String address;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}