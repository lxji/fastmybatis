package com.myapp;

import com.gitee.fastmybatis.core.query.Query;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * 1.11.0版本测试
 *
 * @author thc
 */
public class TUserMapperTestV2 extends FastmybatisSpringbootApplicationTests {

    @Autowired
    TUserMapper mapper;

    /**
     * 查询单条数据并返回指定字段
     */
    @Test
    public void getBySpecifiedColumns() {
        Query query = new Query().eq("id", 6);
        TUser tUser = mapper.getBySpecifiedColumns(Arrays.asList("id", "username"), query);
        System.out.println(tUser);
    }


    /**
     * 查询单条数据并返回指定字段并转换到指定类中
     */
    @Test
    public void getBySpecifiedColumns2() {
        Query query = new Query().eq("id", 6);
        UserVO userVo = mapper.getBySpecifiedColumns(Arrays.asList("id", "username"), query, UserVO.class);
        System.out.println(userVo);
    }

    /**
     * 查询某个字段值
     */
    @Test
    public void getColumnValue() {
        Query query = new Query().eq("id", 6);
        String username = mapper.getColumnValue("username", query, String.class);
        System.out.println(username);
    }

    /**
     * 查询指定列，返指定列集合
     */
    @Test
    public void listColumnValues() {
        // id > 10
        Query query = new Query().gt("id", 10);
        List<String> list = mapper.listColumnValues("username", query, String.class);
        System.out.println(list);
    }

    /**
     * 根据多个主键值查询
     */
    @Test
    public void listByIds() {
        List<TUser> tUsers = mapper.listByIds(Arrays.asList(6, 7));
        for (TUser tUser : tUsers) {
            System.out.println(tUser);
        }
    }

    /**
     * 保存或更新（所有字段）
     */
    @Test
    public void saveOrUpdate() {
        TUser user = new TUser();
        user.setId(201);
        user.setIsdel(false);
        user.setLeftMoney(22.1F);
        user.setMoney(new BigDecimal("100.5"));
        user.setRemark("备注");
        user.setState((byte)0);
        user.setUsername("张三2");
        mapper.saveOrUpdate(user);
        System.out.println(user);
    }

    /**
     * 保存或更新（不为null的字段）
     */
    @Test
    public void saveOrUpdateIgnoreNull() {
        TUser user = new TUser();
        user.setId(20);
        user.setIsdel(false);
        user.setState((byte)0);
        user.setUsername("张三2");
        mapper.saveOrUpdateIgnoreNull(user);
        System.out.println(user);
    }

    /**
     * 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
     */
    @Test
    public void deleteByIds() {
        int i = mapper.deleteByIds(Arrays.asList(1,2,3));
        print("del --> " + i);
    }

    /**
     * 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作
     */
    @Test
    public void deleteByColumn() {
        int i = mapper.deleteByColumn("username", Arrays.asList("username0", "tom"));
        print("del --> " + i);
        i = mapper.deleteByColumn("username", "jim");
        print("del --> " + i);
    }

    @Test
    public void saveUnique() {
        List<TUser> users = new ArrayList<>();
        Date date = new Date();
        for (int i = 0; i < 3; i++) { // 创建3个重复对象
            TUser user = new TUser();
            user.setUsername("username" + 1);
            user.setMoney(new BigDecimal(1));
            user.setRemark("remark" + 1);
            user.setState((byte)0);
            user.setIsdel(false);
            user.setAddTime(date);
            user.setLeftMoney(200F);
            users.add(user);
        }
        mapper.saveUnique(users);
    }

    @Test
    public void saveUnique2() {
        List<TUser> users = new ArrayList<>();
        Date date = new Date();
        for (int i = 0; i < 3; i++) { // 创建3个重复对象
            TUser user = new TUser();
            user.setUsername("username" + (i % 2));
            user.setMoney(new BigDecimal(1));
            user.setRemark("remark" + 1);
            user.setState((byte)0);
            user.setIsdel(false);
            user.setAddTime(date);
            user.setLeftMoney(200F);
            users.add(user);
        }
        mapper.saveUnique(users, Comparator.comparing(TUser::getUsername));
    }

}
