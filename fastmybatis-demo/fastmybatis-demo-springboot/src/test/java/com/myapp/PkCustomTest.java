package com.myapp;

import com.myapp.dao.LogCustomMapper;
import com.myapp.entity.LogCustom;
import com.myapp.entity.LogUuid;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class PkCustomTest extends FastmybatisSpringbootApplicationTests {

    @Autowired
    private LogCustomMapper logCustomMapper;

    @Test
    public void testSave() {
        LogCustom logUuid = new LogCustom();
        logUuid.setLogId(String.valueOf(System.currentTimeMillis()));
        logUuid.setContent("aaa");
        logCustomMapper.saveIgnoreNull(logUuid);
    }
}
