package com.myapp;

import com.myapp.dao.LogUuidMapper;
import com.myapp.entity.LogUuid;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PkUuidTest extends FastmybatisSpringbootApplicationTests {

    @Autowired
    private LogUuidMapper logUuidMapper;


    /**
     * 主键uuid
     */
    @Test
    public void testSave() {
        LogUuid logUuid = new LogUuid();
        logUuid.setContent("aaa");
        logUuidMapper.saveIgnoreNull(logUuid);
        System.out.println(logUuid);
    }
}
