package com.myapp;

import com.myapp.entity.TUser;
import com.myapp.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author thc
 */
public class UserServiceTest extends FastmybatisSpringbootApplicationTests{

    @Autowired
    private UserService userService;

    @Test
    public void test() {
        TUser user = userService.getById(6);
        System.out.println(user);
    }
}
