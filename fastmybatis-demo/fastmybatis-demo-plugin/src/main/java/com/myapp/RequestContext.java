package com.myapp;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author tanghc
 */
public class RequestContext extends ConcurrentHashMap<String, Object> {

    protected static final ThreadLocal<? extends RequestContext> THREAD_LOCAL = ThreadLocal.withInitial(RequestContext::new);

    private static final String KAY_INDEX = "key_index";

    /**
     * Get the current RequestContext
     *
     * @return the current RequestContext
     */
    public static RequestContext getCurrentContext() {
        return THREAD_LOCAL.get();
    }

    public void setIndex(int index) {
        put(KAY_INDEX, index);
    }

    public Integer getIndex() {
        return (Integer)get(KAY_INDEX);
    }

    public void reset() {
        THREAD_LOCAL.remove();
    }
}
