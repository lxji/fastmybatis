
CREATE DATABASE IF NOT EXISTS `stu` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE `stu`;




DROP TABLE IF EXISTS `user_log3`;
DROP TABLE IF EXISTS `user_log2`;
DROP TABLE IF EXISTS `user_log1`;
DROP TABLE IF EXISTS `user_log0`;


CREATE TABLE `user_log0` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_log1` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_log2` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_log3` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT COMMENT '注释',
  `user_id` int(10) DEFAULT NULL COMMENT '注释',
  `log` varchar(200) DEFAULT NULL COMMENT '注释',
  `log_date` datetime DEFAULT NULL COMMENT '注释',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '注释',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

