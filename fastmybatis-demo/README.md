## fastmybatis-demo

fastmybatis的demo

- fastmybatis-demo-springboot：整合springboot
- fastmybatis-demo-springmvc：整合springmvc
- springboot-multi-datasource：整合springboot多数据源
- mutil-module：演示多模块
- tenant-demo：演示多租户，同一数据库，同一张表，通过字段区分
- tenant-demo-table：演示多租户，同一数据库，不同表
- fastmybatis-demo-plugin：演示插件（分表）
