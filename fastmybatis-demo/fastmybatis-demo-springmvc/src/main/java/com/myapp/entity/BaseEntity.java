package com.myapp.entity;

import java.util.Date;

/**
 * 实体类基类，封装通用字段，泛型参数表示主键类型如，Integer
 * @author tanghc
 */
public class BaseEntity<T> {

    private T id;

    /** 添加时间, 数据库字段：create_time */
    private Date createTime;

    /** 修改时间, 数据库字段：update_time */
    private Date updateTime;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
