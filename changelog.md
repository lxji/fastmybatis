# 更新日志

## 2.0.0（对1.x不兼容）

- 新增`ActiveRecord`模式
- 移除JPA注解，采用自带注解，精简依赖
- 移除标记为废弃的方法：`listMap`、`pageMap`、`saveMulti`、`updateByQuery(Map<String, ?> setBlock, Query query)`
- 移除`fastmybatis-generator`模块，由 [code-gen](https://gitee.com/durcframework/code-gen) 代替
- 移除`BaseService`模块，由`IService`代替

**1.x升级到2.0**

实体类进行全局替换：

`javax.persistence.Table` 替换成：`com.gitee.fastmybatis.annotation.Table`

`javax.persistence.Column` 替换成：`com.gitee.fastmybatis.annotation.Column`

移除：`javax.persistence.Transient`注解，改成`transient`关键字

移除：`javax.persistence.Id`注解，改成：`@Table(name = "t_user", pk = @Pk(name = "id"))`

移除：`javax.persistence.GeneratedValue`注解，改成：`@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))`

## 1.11.1

- 新增`int saveUnique(Collection<E> entitys)` 批量保存并去重
- 新增`int saveUnique(Collection<E> entitys, Comparator<E> comparator) ` 批量保存并去重，指定比较器
- 新增`BaseService`通用service
- 新增`IService`通用接口

## 1.11.0

- mapper新增如下方法：
    - `E getBySpecifiedColumns(List<String> columns, Query query)` 查询单条数据并返回指定字段
    - `<T> T getBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)` 查询单条数据并返回指定字段并转换到指定类中
    - `List<E> listByIds(Collection<I> ids)` 根据多个主键值查询
    - `T getColumnValue(String column, Query query, Class<T> clazz)` 查询某个字段值
    - `List<T> listColumnValues(String column, Query query, Class<T> clazz)` 查询指定列，返指定列集合
    - `int saveOrUpdate(E entity)` 保存或更新（所有字段）
    - `int saveOrUpdateIgnoreNull(E entity)` 保存或更新（不为null的字段）
    - `int deleteByIds(Collection<I> ids)` 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
    - `int deleteByColumn(String column, Object value)` 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作

## 1.10.12

- Query类新增条件表达式`query.eq(StringUtils.hasText(name), "name", name);`
- 修复mysql下tinyint(1)返回boolean值问题
- 检查空字符串默认进行一次trim。`mybatis.empty-string-with-trim=true`
- 查询字段忽略空字符调整为默认开启`mybatis.ignore-empty-string=true`

## 1.10.11

- 修复mapper查询属性拷贝问题

## 1.10.10 （有BUG，勿用）

- dom4j升级到2.1.3
- mapper新增page重载方法，方便类型转换

## 1.10.9

- mybatis升级到3.5.7
- mybatis-spring升级到2.0.6

## 1.10.8

- 修复无主键的表查询多一个字段问题

## 1.10.7

- 支持查询返回指定列并分页 `mapper.pageBySpecifiedColumns`

## 1.10.6

- 支持未设置主键表

## 1.10.5

- mapper新增分页查询方法，支持easyui表格
- 优化条件忽略设置
- starter新增`mybatis.ignore-empty-string=true`配置

## 1.10.4

- Query新增`sql(String, Object...)`方法

## 1.10.3

- 增强`SchMapper.listBySpecifiedColumns(List<java.lang.String>, Query, Class<T>)`方法，Class支持基本类型class。

使用示例：

```java
    // 返回id列
    List<Integer> idList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, Integer.class/* 或int.class */);
    for (Integer id : idList) {
        System.out.println(id);
    }

    // 返回id列，并转换成String
    List<String> strIdList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, String.class);

    for (String id : strIdList) {
        System.out.println("string:" + id);
    }

    // 返回username列
    List<String> usernameList = mapper.listBySpecifiedColumns(Collections.singletonList("username"), query, String.class);
    for (String username : usernameList) {
        System.out.println(username);
    }

    // 返回时间列
    List<Date> dateList = mapper.listBySpecifiedColumns(Collections.singletonList("add_time"), query, Date.class);
    for (Date date : dateList) {
        System.out.println(date);
    }

    // 返回decimal列
    List<BigDecimal> moneyList = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, BigDecimal.class);
    for (BigDecimal money : moneyList) {
        System.out.println(money);
    }

    // 返回decimal列并转成float
    List<Float> moneyList2 = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, float.class);
    for (Float money : moneyList2) {
        System.out.println("float:" + money);
    }

    // 返回tinyint列
    List<Byte> stateList = mapper.listBySpecifiedColumns(Collections.singletonList("state"), query, byte.class);
    for (Byte state : stateList) {
        System.out.println("state:" + state);
    }
```

## 1.10.2

- Mapper新增以下三个方法：
 - `listBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)`：查询返回指定的列，并将结果转换成Class对应的类
 - `listBySpecifiedColumns(List<String> columns, Query query)`：查询返回指定的列，返回实体类集合
 - `PageInfo<E> page(Query query)` 分页查询，返回分页信息

## 1.10.1

- 修复下划线字段映射问题

## 1.10.0

- Mapper新增forceDelete、forceDeleteById、forceDeleteByQuery、forceById四个方法
- deleteByQuery可检测逻辑删除字段

## 1.9.2

- 级联查询忽略逻辑删除字段

## 1.9.1

- 去除fastjson，改为gson

## 1.9.0（Java8）

- 支持LocalDateTime

## 1.8.6

- 修复saveIgnoreNull自定义填充器失效问题

## 1.8.5

- 修复updateIgnoreNull&saveIgnoreNull问题

## 1.8.4

- 修复热部署空指针问题
- 修复oracle下saveMulti问题

## 1.8.3

- 修复oracle下getByQuery随机取一条问题

## 1.8.2

- 支持oracle sequence

## 1.8.1

- 修复ignoreUpdateColumns字段失效问题

## 1.8.0

- 全局指定忽略更新字段
- 实体类支持泛型主键

## 1.7.4

- 修复oracle下的BUG

## 1.7.3

- 优化自定义主键策略回写id值到实体类

## 1.7.2

- 修复条件删除BUG

## 1.7.1

- 修复Condition注解空指针异常

## 1.7.0

- 支持热部署，修改xml无需重启

## 1.6.0

- @Condition新增handlerClass属性
- @Condition新增ignoreValue属性

## 1.5.1

- @Condition新增index属性优化

## 1.5.0 

- @Condition新增index属性，调整条件顺序
- @新增LazyFetch注解
- 修复oracle数据库插入问题

## 1.4.2

- 修复懒加载BUG

## 1.4.1

- mapper.updateByQuery()方法支持逻辑删除字段

## 1.4.0

- 新增实体类一对一懒加载功能
- 查询BUG修复

## 1.3.0(有BUG不要使用此版本)

- updateByQuery可强制更新null
- 修复distinct使用方式id顺序问题
- @Condition注解可作用在字段上

## 1.2.0

- 修复自定义xml文件合并BUG
- 新增updateByMap方法
- 新增distinct使用方式

## 1.1.1

- 修复实体类静态字段被解析问题

## 1.1.0

- 提供easyui表格分页查询，见MapperUtil.java

## 1.0.17

- 修复SqlServer模板问题（使用SqlServer数据库必须升级到此版本）


## 1.0.16

- @Condition注解新增ignoreEmptyString属性 https://durcframework.gitee.io/fastmybatis/#27040701

## 1.0.15

- 代码优化性能优化（阿里代码规范+sonar）

## 1.0.14

- 代码优化（sonar）

## 1.0.13

- 合并PR2，https://gitee.com/durcframework/fastmybatis/pulls/2

## 1.0.12

- 修复模板问题


## 1.0.11

- 优化属性拷贝

## 1.0.10

- 增强Mapper.xml，不同Mapper文件可指定同一个namespace，最终会合并

## 1.0.9

- 对象转换方法优化

## 1.0.8

- pageSize传0时报错bug

## 1.0.7

- 参数类构建条件可以获取父类属性

## 1.0.6

- EasyuiDtagridParam增加条件忽略

## 1.0.5

- easyui表格参数支持

## 1.0.4

- Mapper对应无实体类BUG

## 1.0.3

- 添加强制查询功能，见TUserMapperTest.testForceQuery()

## 1.0.2

- 完善注释
- 代码优化

## 1.0.1 初版