package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.util.ClassUtil;
import org.junit.Test;

/**
 * @author thc
 */
public class ClassUtilTest {

    @Test
    public void test() {
        System.out.println(ClassUtil.getSuperInterfaceGenericType(B.class, 0));
        System.out.println(ClassUtil.getSuperInterfaceGenericType(B.class, 0));
    }

    interface S<T> {}
    class A {}
    class B implements S<A> {}

}
