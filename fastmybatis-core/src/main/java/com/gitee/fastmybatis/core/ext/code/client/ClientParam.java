package com.gitee.fastmybatis.core.ext.code.client;

import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.util.ClassUtil;
import org.springframework.core.io.Resource;

/**
 * 客户端参数
 * @author tanghc
 */
public class ClientParam {
	private Class<?> mapperClass;
	/** 模板资源 */
	private Resource templateResource;
	private String globalVmLocation;
	private FastmybatisConfig config;

	public Class<?> getEntityClass() {
		if (mapperClass.isInterface()) {
			return ClassUtil.getSuperInterfaceGenericType(mapperClass, 0);
		} else {
			return ClassUtil.getSuperClassGenricType(mapperClass, 0);
		}
	}
	
	public String getGlobalVmPlaceholder() {
		return this.config.getGlobalVmPlaceholder();
	}

	public String getGlobalVmLocation() {
		return globalVmLocation;
	}

	public void setGlobalVmLocation(String globalVmLocation) {
		this.globalVmLocation = globalVmLocation;
	}

	public Class<?> getMapperClass() {
		return mapperClass;
	}

	public void setMapperClass(Class<?> mapperClass) {
		this.mapperClass = mapperClass;
	}

	public FastmybatisConfig getConfig() {
		return config;
	}

	public void setConfig(FastmybatisConfig config) {
		this.config = config;
	}

	public String getCountExpression() {
		return this.config.getCountExpression();
	}

	public Resource getTemplateResource() {
		return templateResource;
	}

	public void setTemplateResource(Resource templateResource) {
		this.templateResource = templateResource;
	}
}
