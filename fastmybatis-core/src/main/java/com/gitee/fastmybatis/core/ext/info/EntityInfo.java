package com.gitee.fastmybatis.core.ext.info;

/**
 * 实体类信息
 *
 * @author thc
 */
public class EntityInfo {

    /**
     * 主键对应的数据库字段名称
     */
    private String pkColumnName;

    /**
     * 主键对应的JAVA字段名称
     */
    private String pkJavaName;


    public String getPkColumnName() {
        return pkColumnName;
    }

    public void setPkColumnName(String pkColumnName) {
        this.pkColumnName = pkColumnName;
    }

    public String getPkJavaName() {
        return pkJavaName;
    }

    public void setPkJavaName(String pkJavaName) {
        this.pkJavaName = pkJavaName;
    }
}
