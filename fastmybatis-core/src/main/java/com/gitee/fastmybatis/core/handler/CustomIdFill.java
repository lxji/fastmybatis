package com.gitee.fastmybatis.core.handler;

import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;

import java.lang.reflect.Field;

/**
 * 自定义ID填充器，实现自定义ID生成策略，需要继承这个类。
 *
 * @author tanghc
 */
public abstract class CustomIdFill<T> extends BaseFill<T> {

    @Override
    public boolean match(Class<?> entityClass, Field field, String columnName) {
        return FieldUtil.isPkStrategyNone(field, getFastmybatisConfig());
    }

    @Override
    public FillType getFillType() {
        return FillType.INSERT; // INSERT时触发
    }

    @Override
    protected Object buildFillValue(T parameter) {
        Object val = super.buildFillValue(parameter);
        Identitys.set(val);
        return val;
    }
}
