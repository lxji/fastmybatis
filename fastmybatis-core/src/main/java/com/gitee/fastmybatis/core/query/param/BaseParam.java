package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.query.TenantQuery;
import com.gitee.fastmybatis.core.query.Query;

import java.io.Serializable;

/**
 * @author tanghc
 */
public class BaseParam implements Serializable {

	/** 
	 * 生成Query查询对象
	 * @return 返回查询对象
	 */
	public Query toQuery() {
		return new Query().addAnnotionExpression(this);
	}

	/**
	 * 生成TenantQuery查询对象
	 * @return 返回TenantQuery
	 */
	public TenantQuery toTenantQuery() {
		TenantQuery dynamicQuery = new TenantQuery();
		dynamicQuery.addAnnotionExpression(this);
		return dynamicQuery;
	}
}
