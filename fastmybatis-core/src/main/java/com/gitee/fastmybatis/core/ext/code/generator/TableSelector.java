package com.gitee.fastmybatis.core.ext.code.generator;

import com.gitee.fastmybatis.annotation.Table;
import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;
import org.springframework.util.StringUtils;

import java.util.Optional;

/**
 * 表选择
 *
 * @author tanghc
 */
public class TableSelector {

    private final ColumnSelector columnSelector;
    private final Class<?> entityClass;
    private final FastmybatisConfig config;

    public TableSelector(Class<?> entityClass, FastmybatisConfig config) {
        if (config == null) {
            throw new IllegalArgumentException("FastmybatisConfig不能为null");
        }
        if (entityClass == null) {
            throw new IllegalArgumentException("entityClass不能为null");
        }
        this.entityClass = entityClass;
        this.config = config;
        this.columnSelector = new ColumnSelector(entityClass, config);
    }

    public TableDefinition getTableDefinition() {
        TableDefinition tableDefinition = new TableDefinition();
        String tableName = getTableName();
        tableDefinition.setTableName(tableName);
        tableDefinition.setColumnDefinitions(columnSelector.getColumnDefinitions());
        tableDefinition.setAssociationDefinitions(columnSelector.getAssociationDefinitions());
        return tableDefinition;
    }

    private String getTableName() {
        String tableName = null;
        Optional<Table> tableAnnotation = FieldUtil.getTableAnnotation(entityClass);
        if (tableAnnotation.isPresent()) {
            tableName = tableAnnotation.get().name();
        }
        if (StringUtils.isEmpty(tableName)) {
            String javaBeanName = entityClass.getSimpleName();
            if (config.isCamel2underline()) {
                tableName = FieldUtil.camelToUnderline(javaBeanName);
            }
        }
        return tableName;
    }


}
