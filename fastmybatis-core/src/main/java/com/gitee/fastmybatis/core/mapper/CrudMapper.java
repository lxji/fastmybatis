package com.gitee.fastmybatis.core.mapper;

import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.query.Query;

import java.util.Objects;

/**
 * 具备CRUD的mapper
 *
 * @param <E> 实体类，如：Student
 * @param <I> 主键类型，如：Long，Integer
 * @author tanghc
 */
public interface CrudMapper<E, I> extends SchMapper<E, I>, EditMapper<E, I> {

    /**
     * 保存或修改，当数据库存在记录执行UPDATE，否则执行INSERT
     *
     * @param entity 实体类
     * @return 受影响行数
     */
    default int saveOrUpdate(E entity) {
        Objects.requireNonNull(entity, "entity can not null");
        Object pkValue = FastmybatisContext.getPkValue(entity);
        // 如果存在数据，执行更新操作
        if (pkValue != null) {
            String pkColumnName = FastmybatisContext.getPkColumnName(entity.getClass());
            Object columnValue = getColumnValue(pkColumnName, new Query().eq(pkColumnName, pkValue), Object.class);
            if (columnValue != null) {
                return this.update(entity);
            }
        }
        return this.save(entity);
    }

    /**
     * 保存或修改，忽略null字段，当数据库存在记录执行UPDATE，否则执行INSERT
     *
     * @param entity 实体类
     * @return 受影响行数
     */
    default int saveOrUpdateIgnoreNull(E entity) {
        Objects.requireNonNull(entity, "entity can not null");
        Object pkValue = FastmybatisContext.getPkValue(entity);
        // 如果存在数据，执行更新操作
        if (pkValue != null) {
            String pkColumnName = FastmybatisContext.getPkColumnName(entity.getClass());
            Object columnValue = getColumnValue(pkColumnName, new Query().eq(pkColumnName, pkValue), Object.class);
            if (columnValue != null) {
                return this.updateIgnoreNull(entity);
            }
        }
        return this.saveIgnoreNull(entity);
    }

}
