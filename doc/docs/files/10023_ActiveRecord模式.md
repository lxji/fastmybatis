# ActiveRecord模式

实体类实现`com.gitee.fastmybatis.core.support.Record`接口即可拥有ActiveRecord模式

实体类：

```java
/**
 * Active Record
 * 表名：user_info
 * 备注：用户信息表
 */
@Table(name = "user_info")
public class UserInfoRecord implements Record {
    ...
}
```

Mapper类：

```java
public interface UserInfoRecordMapper extends CrudMapper<UserInfoRecord, Integer> {
}
```

测试用例：

```java
    // 保存全部字段
    @Test
    public void save() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.save();
        Assert.assertTrue(success);
    }

    // 保存不为null的字段
    @Test
    public void saveIgnoreNull() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.saveIgnoreNull();
        Assert.assertTrue(success);
    }

    // 修改全部字段
    @Test
    public void update() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setId(4);
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.update();
        Assert.assertTrue(success);
    }

    // 修改不为null的字段
    @Test
    public void updateIgnoreNull() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setId(5);
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.updateIgnoreNull();
        Assert.assertTrue(success);
    }

    // 保存或修改不为null的字段
    @Test
    public void saveOrUpdateIgnoreNull() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.saveOrUpdateIgnoreNull();
        Assert.assertTrue(success);
        System.out.println("id:" + userInfoRecord.getId());
    }

    @Test
    public void saveOrUpdateIgnoreNull2() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setId(8);
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.saveOrUpdateIgnoreNull();
        Assert.assertTrue(success);
    }

    // 删除记录
    @Test
    public void delete() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setId(8);
        boolean success = userInfoRecord.delete();
        Assert.assertTrue(success);
    }
```
