# Mapper详解

fastmybatis中的Mapper提供了一系列增删改查的方法，满足日常所需，完整方法说明如下表所示：

| 方法                                                                                          | 说明                                          | 
|:--------------------------------------------------------------------------------------------|:--------------------------------------------|
| `E getByColumn(String column, Object value)`                                                | 根据字段查询一条记录                                  |
| `E getById(I id)`                                                                           | 根据主键查询                                      |
| `E getByQuery(Query query)`                                                                 | 根据条件查找单条记录                                  |
| `E getBySpecifiedColumns(List<String> columns, Query query)`                                | 查询单条数据并返回指定字段                               |
| `<T> T getBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)`            | 查询单条数据返回指定字段并转换到指定类中                        | 
| `<T> T getColumnValue(String column, Query query, Class<T> clazz)`                          | 查询某一行某个字段值                                  | 
| `long getCount(Query query)`                                                                | 查询总记录数                                      |    
| `List<E> list(Query query)`                                                                 | 查询结果集                                       |     
| `List<E> listByArray(String column, Object[] values)`                                       | 根据多个字段值查询结果集                                | 
| `List<E> listByCollection(String column, Collection<?> values)`                             | 根据字段多个值查询结果集                                | 
| `List<E> listByColumn(String column, Object value)`                                         | 根据字段查询结果集                                   |   
| `List<E> listByIds(Collection<I> ids)`                                                      | 根据多个主键查询                                    |    
| `List<E> listBySpecifiedColumns(List<String> columns, Query query)`                         | 查询返回指定的列，返回实体类集合                            |
| `<T> List<T> listBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)`     | 查询返回指定的列，返指定类集合                             | 
| `<T> List<T> listColumnValues(String column, Query query, Class<T> clazz)`                  | 查询指定列，返指定列集合                                |   
| `PageInfo<E> page(Query query)`                                                             | 分页查询                                        |         
| `<R> PageInfo<R> page(Query query, Function<E, R> converter)`                               | 查询结果集，并转换结果集中的记录，转换处理每一行                    |
| `<R> PageInfo<R> page(Query query, Supplier<R> target, Consumer<R> format)`                 | 查询结果集，并转换结果集中的记录，并对记录进行额外处理                 |
| `<T> PageInfo<T> page(Query query, Supplier<T> target)`                                     | 查询结果集，并转换结果集中的记录                            |       
| `<R> PageInfo<R> pageAndConvert(Query query, Function<List<E>, List<R>> converter)`         | 查询结果集，并转换结果集中的记录，转换处理list                   |    
| `<T> PageInfo<T> pageBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)` | 查询返回指定的列，返回分页数据                             |        
| `PageEasyui<E> pageEasyui(Query query)`                                                     | 查询返回easyui结果集                               |             
| `<T> PageEasyui<T> pageEasyui(Query query, Class<T> clazz)`                                 | 查询返回easyui结果集，并转换结果集中的记录                    |      
| `E forceById(I id)`                                                                         | 根据主键查询强制查询，忽略逻辑删除字段                         |     
| `int save(E entity)`                                                                        | 保存，保存所有字段                                   |           
| `int saveBatch(Collection<E> entitys)`                                                      | 批量保存                                        |              
| `int saveIgnoreNull(E entity)`                                                              | 保存，忽略null字段                                 |            
| `int saveMultiSet(Collection<E> entitys)`                                                   | 批量保存,兼容更多的数据库版本,忽略重复行，此方式采用union的方式批量insert |
| `int saveOrUpdate(E entity)`                                                                | 保存或修改，当数据库存在记录执行UPDATE，否则执行INSERT           |     
| `int saveOrUpdateIgnoreNull(E entity)`                                                      | 保存或修改，忽略null字段，当数据库存在记录执行UPDATE，否则执行INSERT  |    
| `int saveUnique(Collection<E> entitys)`                                                     | 批量保存，去除重复行，通过对象是否相对判断重复数据，实体类需要实现equals方法   |
| `int saveUnique(Collection<E> entitys, Comparator<E> comparator)`                           | 批量保存，去除重复行，指定比较器判断                          |           
| `int update(E entity)`                                                                      | 更新，更新所有字段                                   |                 
| `int updateByQuery(E entity, Query query)`                                                  | 根据条件更新                                      |                   
| `int updateIgnoreNull(E entity)`                                                            | 更新，忽略null字段                                 |                  
| `int updateByMap(Map<String, Object> map, Query query)`                                     | 根据条件更新，map中的数据转化成update语句set部分，key为数据库字段名   |      
| `int delete(E entity)`                                                                      | 删除，在有逻辑删除字段的情况下，做UPDATE操作                   | 
| `int deleteByColumn(String column, Object value)`                                           | 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作            |
| `int deleteById(I id)`                                                                      | 根据id删除，在有逻辑删除字段的情况下，做UPDATE操作               |   
| `int deleteByIds(Collection<I> ids)`                                                        | 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作           | 
| `int deleteByQuery(Query query)`                                                            | 根据条件删除，在有逻辑删除字段的情况下，做UPDATE操作               |  
| `int forceDelete(E entity)`                                                                 | 强制删除（底层根据id删除），忽略逻辑删除字段，执行DELETE语句          |
| `int forceDeleteById(I id)`                                                                 | 根据id强制删除，忽略逻辑删除字段，执行DELETE语句                |    
| `int forceDeleteByQuery(Query query)`                                                       | 根据条件强制删除，忽略逻辑删除字段，执行DELETE语句                |   

